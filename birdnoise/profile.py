from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
    )

from birdnoise.auth import login_required
from birdnoise.db import get_db

bp = Blueprint('profile', __name__)

