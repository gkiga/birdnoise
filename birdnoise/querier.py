from birdnoise.db import get_db

import time

backend = "sqlite"

def register_user(first_name, last_name, email, password_hash):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO user (email, password, first_name, last_name) VALUES (%s,%s,%s,%s)',
        (email, password_hash,first_name, last_name))
    get_db().commit()

def get_user_by_email(email):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT * FROM user WHERE email = %s', (email,)
        )
    return db.fetchone()

def get_user_by_id(user_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT * FROM user WHERE id = %s', (user_id,)
        )
    return db.fetchone()

def get_all_posts_and_reposts():
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT '
        'p.id as post_id, '
        'r.id as repost_id, '
        'r.time as post_time '
        'FROM repost r '
        'INNER JOIN post p ON r.post_id=p.id '
        'UNION '
        'SELECT '
        'p.id as post_id, '
        'NULL as repost_id, '
        'p.time as post_time '
        'FROM post p '
        'ORDER BY post_time DESC',
        )
    return db.fetchall()

def get_all_following_posts(user_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT following_id '
        'FROM follower '
        'WHERE follower_id=%s',
        (user_id,))
    followlist = [f['following_id'] for f in db.fetchall()]
    followlist.append(user_id)
    print(followlist)
    query_string = \
        'SELECT '\
        'p.id as post_id, ' \
        'r.id as repost_id, ' \
        'r.time as post_time ' \
        'FROM repost r ' \
        'INNER JOIN post p ON r.post_id=p.id ' \
        'WHERE r.user_id IN (%s) ' \
        'UNION ALL ' \
        'SELECT ' \
        'p.id as post_id, ' \
        'NULL as repost_id, ' \
        'p.time as post_time ' \
        'FROM post p ' \
        'WHERE p.user_id IN (%s) ' \
        'ORDER BY post_time DESC' \
        % (','.join(["%s"] * len(followlist)),','.join(["%s"] * len(followlist)))

    db.execute(query_string,
        followlist + followlist)
    return db.fetchall()
        
def get_user_posts_and_reposts(user_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT '
        'p.id as post_id, '
        'p.user_id as post_user, '
        'op.first_name as first_name, '
        'op.last_name as last_name, '
        'r.time as post_time, '
        'p.content as content, '
        'r.user_id as reposter, '
        'rep.first_name as r_first_name, '
        'rep.last_name as r_last_name '
        'FROM post p '
        'INNER JOIN repost r ON r.post_id=p.id '
        'INNER JOIN user rep ON rep.id=r.user_id '
        'INNER JOIN user op ON op.id=p.user_id '
        'WHERE r.user_id=%s '
        'UNION '
        'SELECT '
        'p.id as post_id, '
        'p.user_id as post_user, '
        'u.first_name as first_name, '
        'u.last_name as last_name, '
        'p.time as post_time, '
        'p.content as content, '
        'null as reposter, '
        'null as r_first_name, '
        'null as r_last_name '
        'FROM post p '
        'INNER JOIN user u ON u.id=p.user_id '
        'WHERE p.user_id=%s '
        'ORDER BY post_time DESC',
        (user_id, user_id))
    return db.fetchall()

def get_post(post_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT '
        'p.id as post_id, '
        'p.user_id as post_user, '
        'u.first_name as first_name, '
        'u.last_name as last_name, '
        'p.time as post_time, '
        'p.content as content '
        'FROM post p '
        'INNER JOIN user u ON u.id=p.user_id '
        'WHERE p.id=%s',
        (post_id,))
    return db.fetchone()

def get_repost(repost_id):
    db = get_db().cursor(dictionary=True,buffered=True)
    db.execute(
        'SELECT '
        'p.id as post_id, '
        'p.user_id as post_user, '
        'u.first_name as first_name, '
        'u.last_name as last_name, '
        'r.user_id as r_user, '
        'rep.first_name as r_first_name, '
        'rep.last_name as r_last_name, '
        'r.time as post_time, '
        'p.content as content '
        'FROM repost r '
        'INNER JOIN post p ON p.id=r.post_id '
        'INNER JOIN user u ON u.id=p.user_id '
        'INNER JOIN user rep ON rep.id=r.user_id '
        'WHERE r.id=%s',
        (repost_id,))
    return db.fetchone()

def make_post(user_id, content):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO post (user_id, time, content) '
        'VALUES (%s,%s,%s)',
        (user_id, int(time.time()), content))
    get_db().commit()

def make_repost(user_id, post_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO repost (user_id, post_id, time) '
        'VALUES (%s,%s,%s)',
        (user_id, post_id, int(time.time())))
    get_db().commit()

def make_comment(user_id, post_id, content):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO comment (user_id, post_id, time, content) '
        'VALUES (%s,%s,%s,%s)',
        (user_id, post_id, int(time.time()), content))
    get_db().commit()

def make_like(user_id, post_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO `like`(user_id, post_id) '
        'VALUES (%s,%s)',
        (user_id, post_id))
    get_db().commit()

def remove_like(user_id, post_id):
    db = get_db().cursor(dictionary=True)
    db.execute('DELETE FROM `like` '
               'WHERE user_id=%s AND post_id=%s',
               (user_id, post_id))
    get_db().commit()

def get_like(user_id, post_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT 1 '
        'FROM `like` '
        'WHERE user_id=%s AND post_id=%s',
        (user_id, post_id))
    return db.fetchone() != None

def get_follow(follower, following):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT 1 '
        'FROM `follower` '
        'WHERE follower_id=%s AND following_id=%s',
        (follower, following))
    return db.fetchone() != None

def get_likes(post_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT COUNT(user_id) as like_count '
        'FROM `like` '
        'WHERE post_id=%s',
        (post_id,))
    return db.fetchone()['like_count']

def make_comment(user_id, post_id, content):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO [comment] (user_id, post_id, time, content) '
        'VALUES (%s,%s,%s,%s)',
        (user_id, post_id, int(time.time()), content))
    get_db().commit()

def get_comments(post_id, start, end):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT '
        'c.user_id as comment_id, '
        'c.post_id as comment_id, '
        'c.time as comment_time, '
        'c.content as content '
        'FROM `comment` c '
        'WHERE c.post_id=%s',
        (post_id,))
    return db.fetchall()

def make_follow(following, follower):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO `follower` '
        '(follower_id, following_id) '
        'VALUES (%s,%s)',
        (follower,following))
    get_db().commit()

def remove_follow(following, follower):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'DELETE FROM `follower` '
        'WHERE follower_id=%s '
        'AND following_id=%s',
        (follower,following))
    get_db().commit()
