from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
    )
from werkzeug.exceptions import abort
from birdnoise.auth import login_required
from birdnoise.db import get_db
from birdnoise.querier import *

import time

bp = Blueprint('timeline', __name__)

@bp.route('/')
def index():
    posts = []
    if g.user:
        posts = get_all_following_posts(g.user['id'])
    else:
        posts = get_all_posts_and_reposts()
    print(posts)
    return render_template('timeline.html', posts=posts)

@bp.route('/profile/<int:id>')
def profile(id):
    user = get_user_by_id(id)
    if not user:
        abort(404, "User does not exist")
    following = False
    if g.user:
        following = get_follow(g.user['id'], id)

    posts = get_user_posts_and_reposts(id)
    return render_template('profile.html',
                           following=following,
                           user=user, posts=posts)

@bp.route('/profile/<int:id>/follow')
@login_required
def follow(id):
    following = get_user_by_id(id)
    if not following:
        abort(404, "User does not exist")

    follower = g.user['id']
    make_follow(id, follower)
    return redirect(url_for('timeline.profile', id=id))

@bp.route('/profile/<int:id>/unfollow')
@login_required
def unfollow(id):
    following = get_user_by_id(id)
    if not following:
        abort(404, "User does not exist")

    follower = g.user['id']
    remove_follow(id, follower)
    return redirect(url_for('timeline.profile', id=id))

@bp.route('/submit', methods=('POST',))
@login_required
def submit():
    flash('submitted')
    content = request.form['content']
    user_id = g.user['id']

    make_post(user_id, content)
    return redirect(url_for('timeline.index'))

@bp.route('/repost/<int:id>')
@login_required
def repost(id):
    flash('reposted')
    make_repost(g.user['id'], id)
    
    return redirect(url_for('timeline.index'))

@bp.route('/post/<int:id>/like')
@login_required
def like(id):
    user_id = g.user['id']
    try:
        make_like(user_id, id)
    except Exception as inst:
        pass
    return redirect(url_for('timeline.post', id=id))

@bp.route('/post/<int:id>/unlike')
@login_required
def unlike(id):
    user_id = g.user['id']
    try:
        remove_like(user_id, id)
    except Exception as inst:
        pass
    return redirect(url_for('timeline.post', id=id))

@bp.route('/post/<int:id>')
def post(id):
    post = get_post(id)
    comments = get_comments(id, 0,0)
    is_liked = False
    if (g.user):
        is_liked=get_like(g.user['id'], id)
    if not post:
        abort(404, "Post does not exist")
    return render_template('post.html',
                           post=post,
                           comments=comments,
                           like_count=get_likes(id),
                           is_liked=is_liked)

@bp.route('/post/<int:id>/comment', methods=('POST',))
@login_required
def comment(id):
    content = request.form['content']
    user_id = g.user['id']

    make_comment(user_id, id, content)
    return redirect(url_for('timeline.post', id=id))

@bp.route('/post/<int:id>/view')
def post_view(id):
    post = get_post(id)
    is_liked = False
    if (g.user):
        is_liked=get_like(g.user['id'], id)
    if not post:
        abort(404, "Post does not exist")
    return render_template('post_view.html',
                           post=post,
                           like_count=get_likes(id),
                           is_liked=is_liked)

@bp.route('/repost/<int:id>/view')
def repost_view(id):
    post = get_repost(id)
    like_count = get_likes(post['post_id'])
    is_liked = False
    if (g.user):
        is_liked=get_like(g.user['id'], post['post_id'])
    if not post:
        abort(404, "Post does not exist")
    return render_template('post_view.html',
                           post=post,
                           like_count=like_count,
                           is_liked=is_liked)
